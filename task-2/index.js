
// TASK 2.6

function getDaysForMonth(month) {
    switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 9:
        case 11:
            return 31;
        case 4:
        case 6:
        case 8:
        case 10:
        case 12:
            return 30
        case 2:
            return 28;

    }

// TASK 2.5

}
function getDiscount(number) {
    if (number < 5) {
        return 100;
    }
    else if (number > 4 && number < 10) {
        return 95;
    }
    else if (number > 10) {
        return 90
    }
}

// TASK 2.4

function arrayUnicNumber(array) {
    if (array[0] != array[1]) {
        if (array[0] == array[2]) {
            return array[1];
        }
        else {
            return array[0];
        }
    } else {
        for (i = 2; i < array.length; i++) {
            if (array[i] != array[0]) {
                return array[i];
            }
        }
    }
}

// TASK 2.3


function dateEqual(date1, date2) {
    const firstDate = new Date(date1);
    const secondDate = new Date(date2);
    if (firstDate.getTime() == secondDate.getTime()) {
        return true;
    }
    else {
        return false;
    }
}

// TASK 2.2 

function  superSort(value) {
  return value.split(" ").sort((a,b) => {
      const first = a.replace(/\d/gm,"")
      const second = b.replace(/\d/gm,"")
      if (first.length == 0) {
          return 1
      }
      if (second.length == 0) {
          return -1
      }
      if ( first < second) {
          return -1
      }
      if (first > second) {
          return 1 
      }
      return -1
    } ).join(" ")
}